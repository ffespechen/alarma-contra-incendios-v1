const int pinLED = 2;
const int pinBUZZER = 3;

void setup()
 {
    
  pinMode(pinLED, OUTPUT); //LED
  pinMode(pinBUZZER, OUTPUT); //BUZZER

  Serial.begin(9600);

 }


 
void loop(){

  int lectura = analogRead(A0);

  Serial.println(lectura);

  while(lectura < 500)
  {
    digitalWrite(pinBUZZER, HIGH);
    
    digitalWrite(pinLED, HIGH);
    delay(500);
    digitalWrite(pinLED, LOW);
    delay(500);
    lectura = analogRead(A0);
  }

  digitalWrite(pinBUZZER, LOW);
  digitalWrite(pinLED, LOW);
  delay(1000);

  }
 
